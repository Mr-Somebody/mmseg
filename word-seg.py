#-*-coding:utf-8-*-


def load_dict():
	'''Load dicts
	The dicts to load contain word dict,unit dict and char dict, which are all gotten from mmseg4j.
	The word dict contains words list, the char dict contains single world frequency and the unit
	dict contains the unit of time, currency, volumn, weight and area.
	Words dict is stored in a key-tree structure.
	'''
	#load word dict
	f=open(DICT_ROOT_DIR+'words.dic')
	for line in f:
		line=unicode(line.rstrip(),CODING)
		cd=word_dict#current dict
		for c in line:
			if not c in cd:
				cd[c]={}
			cd=cd[c]
		cd[END_SYMBLE]=None
	f.close()
	#load chars dict
	f=open(DICT_ROOT_DIR+'chars.dic')
	for line in f:
		line=unicode(line.rstrip(),CODING)
		sl=line.split()
		char_dict[sl[0]]=sl[1]
	f.close()
	#load unit dict
	f=open(DICT_ROOT_DIR+'units.dic')
	for line in f:
		if line.startswith('#'):
			continue
		line=unicode(line.rstrip(),CODING)
		unit_dict[line]=None
	f.close()

def simple_max_length(clause):
	'''Simple max length segment.
	Search the maximum length of word in the word dict from the beginning of the clause.

	Args:
	clause: The clause to search.

	Returns:
	The word cutted and the rest of the clause, as a tuple.
	'''
	word=[]
	cur_dict=word_dict
	for w in clause:
		word.append(w)
		if w in cur_dict:
			cur_dict=cur_dict[w]
		else:
			break
	if len(word)>1:
		del word[-1]
	return (''.join(word),clause[len(word):])

def enum_possible_words(clause):
	'''Enum possible words at the start of the clause
	Find all possible words as the first cutted word of clause in the word dict or unit dict.

	Args:
	clause: The clause to search

	Returns:
	A set of possible words
	'''
	words=set()
	if clause[0] in unit_dict:
		words.add(clause[0])
	if not clause[0] in word_dict:
		words.add(clause[0])
		return words
	word=[clause[0]]
	cur_dict=word_dict[clause[0]]
	for w in clause[1:]:
		if END_SYMBLE in cur_dict:
			words.add(''.join(word))
		if w in cur_dict:
			word.append(w)
			cur_dict=cur_dict[w]
		else:
			words.add(''.join(word))
			break
	words.add(''.join(word))
	return words

def chunk3(clause):
	'''Try chunk 3 segment. Rule-1

	Args:
	clause: The clause to segment

	Returns:
	The chunks found
	'''
	max_chunk3=[0,None]
	for w0 in enum_possible_words(clause):
		for w1 in enum_possible_words(clause[len(w0):]):
			for w2 in enum_possible_words(clause[len(w0)+len(w1):]):
				cl=len(w0)+len(w1)+len(w2)
				if cl>max_chunk3[0]:
					max_chunk3[0]=cl
					max_chunk3[1]=set([(w0,w1,w2)])
				elif cl==max_chunk3[0]:
					max_chunk3.add((w0,w1,w2))
	return max_chunk3[1]

def maximum_average_length(clause):
	'''Maximum average word length of segment. Rule-2
	In all segments, select the segment with maximum average word length.

	Args:
	clause: The clause to separate.

	Returns: The word cutted or None.
	'''
	#[minmum word count, current word count, current word cut, minimum count word set]
	datas=[len(clause),0,None,set()]
	def _recursion(pos=0):
		if pos==len(clause):
			if datas[1]<datas[0]:
				datas[0]=datas[1]
				datas[3].clear()
				datas[3].add(datas[2])
			elif datas[1]==datas[0]:
				datas[3].add(datas[2])
		else:
			if datas[1]==datas[0]:
				return
			for w in enum_possible_words(clause[pos:]):
				if datas[1]==0:
					datas[2]=w
				datas[1]+=1
				_recursion(pos+len(w))
				datas[1]-=1
	_recursion(0)
	if len(datas[3])==1:
		return datas[3].pop()
	return None

def minimum_length_variance(chunks):
	'''Minimum word length variance in chunks. Rule-3
	Select chunks with maximum length, then calaculate their word length variance and choose
	first word of the chunk with minimum variance as the segment result.

	Args:
	clause: Chunks to calculate.

	Returns:
	The word cutted or None.
	'''
	min_variance=1024
	min_var_chunks=set()
	for chunk in chunks:
		average_length=0.0
		for w in chunk:
			average_length+=len(w)
		average_length/=len(chunk)
		var=0
		for w in chunk:
			tmp=len(w)-average_length
			var+=tmp*tmp
		print var
		if var<min_variance:
			min_var_chunks.clear()
			min_var_chunks.add(chunk)
		else:
			min_var_chunks.add(chunk)
	return min_var_chunks

def maximum_morpheme(clause):
	'''Select the chunk with the maximum morpheme. Rule-4
	Search all chunks and select the first word of the chunk with first maximum morpheme.

	Args:
	chunks: Clause to search.

	Returns:
	The word to cut.
	'''
	

def complex_max_length(clause):
	'''Complex max length segment
	'''
	pass
	

def seg(clause):
	'''Clause segment
	Cut a clause into separated words.
	Rules is the same as descripted in page http://leeing.org/2009/11/01/mmseg-chinese-segmentation-algorithm/

	Args:
	clause: The clause to be cutted.

	Returns:
	A bag of words the clause cutted into
	'''
	clause=unicode(clause.strip(),CODING)
	print minimum_length_variance([('123','4','567'),('12','345','67')])
	return []
	words=[]
	while clause:
		word,clause=simple_max_length(clause)
		words.append(word)
	return words

#Root directory of dict
DICT_ROOT_DIR='dict/'
#Encoding of dict and input string
CODING='utf-8'
#End symble in the word dict, which indicate the words before this symble form a word
END_SYMBLE='#'
#If using simple max-length segment
simple=False
#Dicts
word_dict={}
unit_dict={}
char_dict={}
#Load dicts
load_dict()


#test code
import sys
for line in sys.stdin:
	print line
	seg(line)
